var clientesObtenidos;
var rutaBandera1 = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
var rutaBandera2 = ".png";

function getClientes (){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);

      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONClientes.value[0].CompanyName);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log("CompanyName: " + JSONClientes.value[i].CompanyName);

    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].CompanyName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaPais = document.createElement("td");
    var nombrePais = JSONClientes.value[i].Country;
    columnaPais.innerText = nombrePais;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera1 + "United-Kingdom" + rutaBandera2;
    }else{
      imgBandera.src = rutaBandera1 + nombrePais + rutaBandera2;
    }
    //imgBandera.src = rutaBandera1 + nombrePais + rutaBandera2;
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
